xkb-layout
==========

`xkb-layout` is a CLI application to generate templates for a new XKB
layout, either in the user's home directory or the system directory.

Specifically, it provides the templates as outlined in this blog post:
https://who-t.blogspot.com/2020/09/user-specific-xkb-configuration-putting.html


Installation
------------

This tool can be installed with `pip` directly from the git repo, or run
from the git repository.

```
$ git clone https://gitlab.freedesktop.org/whot/xkblayout
$ cd xkblayout
$ xkblayout --layout 'us(newvariant)'
```

Or
```
$ pip install git+http://gitlab.freedesktop.org/whot/xkblayout
```

Usage
-----

This tool takes a `--layout` and/or a `--option` and generates the required
files for this layout/option to be found by libxkbcommon and libxkbregistry.

```
$ xkblayout --layout 'us(newvariant)'
$ xkblayout --option 'custom:foo'
$ xkblayout --layout 'some(other)' --option 'custom:foo'
$ xkblayout --system --layout 'us(other)'
```

By default, this tool installs the template files in `$XDG_CONFIG_HOME/xkb`.
