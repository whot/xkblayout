#!/usr/bin/env python3

from setuptools import setup

setup(name='xkblayout',
      version='1',
      description='XKB layout templating generator',
      long_description=open('README.md', 'r').read(),
      long_description_content_type='text/markdown',
      url='http://gitlab.freedesktop.org/whot/xkb-layout',
      author='Peter Hutterer',
      author_email='peter.hutterer@who-t.net',
      license='GPLv3',
      py_modules=['xkblayout'],
      entry_points={
          'console_scripts': [
              'xkblayout = xkblayout:main',
          ]
      },
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6'
      ],
      data_files=[],  # man pages are added on success
      python_requires='>=3.6',
      include_package_data=True,
)
